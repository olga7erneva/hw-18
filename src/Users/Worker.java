package Users;

public class Worker extends User{
    private double salary;
    private int experience;

    public Worker(String email, String password, double salary, int exp){
        super(email, password);
        this.salary = salary;
        experience=exp;
    };

    public Worker(String firstName, String lastName, int workPhone, int mobilePhone, String email, String password, double salary, int exp){
        super(firstName, lastName, workPhone, mobilePhone, email, password);
        this.salary = salary;
        experience=exp;
    };

    public void riseSalary() {
        if (experience < 2) {
            this.salary *= 1.05;
        } else if (experience >= 2 && experience <= 5) {
            this.salary *= 1.1;
        } else {
            this.salary *= 1.15;
        }
    }
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {this.experience = experience;}
    }

