package Users;

public class User {
    protected String firstName;
    protected String lastName;
    protected int workPhone;
    protected int mobilePhone;
    protected String email;
    protected String password;

    public User(String email, String pass)
    {
        this.firstName = "";
        this.lastName = "";
        this.workPhone = 0;
        this.mobilePhone = 0;
        this.email = email;
        this.password = pass;
    }

    public User(String firstName, String lastName, int workPhone, int mobilePhone, String email, String password)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getLastName() {return  lastName;}

    public int getWorkPhone() {return  workPhone;}

    public int getMobilePhone() {return mobilePhone;}


}