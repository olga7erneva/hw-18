import Users.Worker;

public class Main {

    private static boolean validatePassword(String pass) {
        int passLength = pass.length();
        boolean result = false;
        if (passLength >= 8 && passLength <= 16) {
            result = true;
        } else {
            System.out.println("Password length does not correspond to requirement from 8 to 16 symbols!");
        }

        return result;
    }

    private static boolean validateEmail(String email) {
        boolean result = false;
        if (email.contains("@")) {
            result = true;
        } else {
            System.out.println("Email does not contain @!");
        }

        return result;
    }

       public static void testWorker(String name, String email, String pass, double salary, int exp) {
        System.out.println("----------------------------------------");
        System.out.println(name);

        if (validateEmail(email) && validatePassword(pass)) {
            Worker worker = new Worker(email, pass, salary, exp);

            System.out.println("Test " + name + " Passed!");

            worker.riseSalary();
            System.out.println(email);
            System.out.print("Experience years: ");
            System.out.println(worker.getExperience());
            System.out.print("Salary usual: ");
            System.out.println(salary);
            System.out.print("Salary with bonus: ");
            System.out.println(worker.getSalary());
        } else {
            System.out.println("Test " + name + " Failed!");
        }
    }

    public static void testWorker(String firstName, String lastName, int workPhone, int mobilePhone, String email, String pass, double salary, int exp) {
        System.out.println("----------------------------------------");

        if (validateEmail(email) && validatePassword(pass)) {
            Worker worker = new Worker(firstName, lastName, workPhone, mobilePhone, email, pass, salary, exp);

            System.out.println("Test " + firstName + " Passed!");
            worker.riseSalary();
            System.out.println(lastName);
            System.out.println(workPhone);
            System.out.println(mobilePhone);
            System.out.println(email);
            System.out.print("Experience years: ");
            System.out.println(worker.getExperience());
            System.out.print("Salary usual: ");
            System.out.println(salary);
            System.out.print("Salary with bonus: ");
            System.out.println(worker.getSalary());
        } else {
            System.out.println("Test " + firstName + " Failed!");
        }
    }
    public static void main(String[]args) {
        /// tests for email and password
        testWorker("Ivanov", "Ivan", 1234567, 38023178,"emaildomen.ru", "sdfsf", 0, 1);
        testWorker("Petrov", "Ivan", 1234567, 38023178, "email1@domen.ru", "1sdfsasdadsf", 0, 1);
        testWorker("Sidorov", "Ivan", 1234567, 38024178, "email2@domen.ru", "2sdfsadsdsadasfasafaffffasfafaf", 0, 1);
        /// tests for salary and registrarion with email and password only
        testWorker("Ivanov","email3@domen.ru", "3sdfsasdadsf", 1000, 1);
        testWorker("Petrov", "email4@domen.ru", "4sdfsasdadsf", 1000, 2);
        testWorker("Sidorov", "Pety", 1234567, 3654178, "email5@domen.ru", "5sdfsasdadsf", 1000, 3);
        testWorker("Sidorov", "Nicolay", 1234567, 354178, "email6@domen.ru", "6sdfsasdadsf", 1000, 5);
        testWorker("Petrov", "Nicolay", 1234567, 384178, "email7@domen.ru", "7sdfsasdadsf", 1000, 10);
    }
}

